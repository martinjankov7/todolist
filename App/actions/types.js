export const ADD_TASK 				= 'add_task';
export const NEW_TASK 				= 'new_task';
export const LOAD_TASKS				= 'load_tasks';
export const COMPLETE_TASK 			= 'complete_task';
export const CLEAR_COMPLETED_TASKS 	= 'clear_completed_tasks';