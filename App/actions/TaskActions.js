import { AsyncStorage }  from 'react-native';
import { ADD_TASK, NEW_TASK, COMPLETE_TASK, LOAD_TASKS } from '../actions/types';

export const addTask = (allTasks, task) => {
	let tasks = JSON.parse(allTasks);

	let new_task = {
		id: tasks.length + 1,
		task: task,
		completed: false
	}

	tasks.push(new_task);

	AsyncStorage.setItem('tasks', JSON.stringify(tasks), () => {});

	return (dispatch) => {		
		dispatch({
			type: ADD_TASK,
			payload: JSON.stringify(tasks)
		});
	}
}

export const newTask = (task) => {
	return (dispatch) => {
		dispatch({
			type: NEW_TASK,
			payload: task
		});
	}
}

export const completeTask = (taskID) => {
	return (dispatch) => {
		AsyncStorage.getItem('tasks').then(tasks => {
			tasks = JSON.parse(tasks);
			tasks = tasks.filter(function( task ) {
			    if(task.id == taskID){
			    	task.completed = true;
			    }
			    return task;
			});

			AsyncStorage.setItem('tasks', JSON.stringify(tasks), () => {});

			dispatch({
				type: COMPLETE_TASK,
				payload: tasks
			});
		});
	}
}

export const loadTasks = (tasks) => {
	return (dispatch) => {
		dispatch({
			type: LOAD_TASKS,
			payload: tasks
		});
	}
}