import { ADD_TASK, NEW_TASK, COMPLETE_TASK, LOAD_TASKS } from '../actions/types';

const INITIAL_STATE = { 
	all_tasks: JSON.stringify([]),
	new_task: ''
}

export default (state = INITIAL_STATE, action) => {
	switch(action.type){
		case LOAD_TASKS:
			return { ...state, all_tasks: action.payload }
		case NEW_TASK :
			return { ...state, new_task : action.payload }
		case ADD_TASK :
			return { ...state, all_tasks : action.payload, new_task: '' };
		case COMPLETE_TASK :
			return { ...state, all_tasks : JSON.stringify(action.payload) };
		default : 
			return state;
	}
}