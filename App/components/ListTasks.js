import React, { Component } from 'react';
import { ListView, Text, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import ListItem from './ListItem'; 
import { loadTasks } from '../actions/TaskActions';

class ListTasks extends Component{
	componentWillMount(){
		this.tasks = this.getFromStorage();
	}
	
	renderListItem(data){
		return <ListItem task={data.task} id={data.id}/>
	}

	async getFromStorage(){
		tasks = await AsyncStorage.getItem('tasks');
		this.props.loadTasks(tasks);
	}

	render(){

		if(this.props.tasks.length == 0)
			return <Text style={styles.noTaskTextStyle}>No tasks yet!</Text>

		return (
			<ListView 
				dataSource={ds.cloneWithRows(this.props.tasks)}
				renderRow={this.renderListItem}
			/>
		);
	}
}

const mapStateToProps = (state) => {
	let tasks = JSON.parse(state.tasks.all_tasks);

	tasks = tasks.filter(function( task ) {
		    return !task.completed;
		});

	return { tasks };
}

const styles = {
	noTaskTextStyle : {
		fontSize: 18,
		fontWeight: 'bold',
		alignSelf: 'center'
	}
}

const ds = new ListView.DataSource({
			rowHasChanged : (r1, r2) => r1 !== r2
		});

export default connect(mapStateToProps, { loadTasks })(ListTasks);