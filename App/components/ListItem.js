import React, { Component } from 'react';
import { Text, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { completeTask } from '../actions/TaskActions';

import CardSection from './shared/CardSection'; 

class ListTasks extends Component{
	constructor(props){
		super(props);
	}

	markCompleted(){
		this.props.completeTask(this.props.id);
	}

	render(){
		const { textStyle, buttonStyle, buttonTextStyle } = styles;
		return(
			<CardSection>
				<Text style={textStyle}>{this.props.task}</Text>
				<TouchableHighlight onPress={this.markCompleted.bind(this)} style={buttonStyle}>
					<Text style={buttonTextStyle}>
						Done
					</Text>
			    </TouchableHighlight>
			</CardSection>
		);
	}
}

const styles = {
	textStyle : {
		flex: 4,
		fontSize: 16,
		padding: 5,
		fontWeight: 'bold'
	},
	buttonStyle : {
		flex: 1,
		backgroundColor: '#42B72A',
		padding: 5,
		alignItems: 'center',
		justifyContent: 'center'
	},
	buttonTextStyle : {
		color: '#fff',
		fontSize: 16
	}
}

export default connect(null, { completeTask })(ListTasks);