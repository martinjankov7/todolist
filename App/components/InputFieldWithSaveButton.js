import React, { Component } from 'react';
import { View, TextInput, TouchableHighlight, Text } from 'react-native';
import { connect } from 'react-redux';
import { newTask, addTask } from '../actions/TaskActions';

class InputFieldWithSaveButton extends Component {
	onTaskInput(task){
		this.props.newTask(task);
	}

	onButtonPress(){
		this.props.addTask(this.props.allTasks, this.props.taskValue);
	}

	render(){
		const { textStyle, buttonStyle, viewStyle, buttonTextStyle } = styles;
		return (
			<View style={viewStyle}>
				<TextInput 
					style={textStyle}
					placeholder="Enter task"
					onChangeText={this.onTaskInput.bind(this)}
					value={this.props.taskValue}
				/>
				<TouchableHighlight onPress={this.onButtonPress.bind(this)} style={buttonStyle}>
					<Text style={buttonTextStyle}>
						+
					</Text>
			    </TouchableHighlight>
			</View>
		);
	}
}

const styles = {
	viewStyle : {
		flexDirection: 'row',
		margin: 5,
		padding: 5
	},
	textStyle : {
		flex : 6,
		padding: 5,
		fontSize: 20
	},
	buttonStyle : {
		flex: 1,
		backgroundColor: '#4080ff',
		padding: 5,
		alignItems: 'center',
		justifyContent: 'center'
	},
	buttonTextStyle : {
		color: '#fff',
		fontSize: 24
	}
}

const mapStateToProps = (state) => {
	return { 
		taskValue : state.tasks.new_task,
		allTasks : state.tasks.all_tasks
	}; 
}

export default connect(mapStateToProps, { newTask, addTask })(InputFieldWithSaveButton);