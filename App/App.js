import React, { Component } from 'react';
import { View } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Header from './components/shared/Header';
import Card from './components/shared/Card';
import InputFieldWithSaveButton from './components/InputFieldWithSaveButton';
import ListTasks from './components/ListTasks';

export default class App extends Component{
	render(){
		const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
		return (
			<Provider store={store}>
				<View style={{flex: 1}}>
					<Header headerText="Todo List" />
					<Card>
						 <InputFieldWithSaveButton />
						 <ListTasks />
					</Card>
				</View>
			</Provider>
		);
	}
}